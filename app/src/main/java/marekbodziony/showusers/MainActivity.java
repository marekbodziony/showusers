package marekbodziony.showusers;

import android.support.v7.app.AppCompatActivity;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import java.util.List;

import marekbodziony.showusers.model.SimpleUser;
import marekbodziony.showusers.model.User;
import marekbodziony.showusers.utils.UsersListAdapter;

import static marekbodziony.showusers.utils.Lib.TAG;

/**
 * MainActivity shows specific fragment (list or details) and handles communication between fragments
 */

public class MainActivity extends AppCompatActivity implements UsersListAdapter.UserClickListener{

    private List<SimpleUser> usersList;
    private TextView textView;

    private FragmentManager fragmentManager;
    private ListFragment listFragment;
    private DetailsFragment detailsFragment;

    private boolean showListFragment = false;   // when back is pressed in detailed fragment
    private boolean showMasterDetail;           // on larger screen display list fragment and details fragment together

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (findViewById(R.id.master_detail_layout) != null) showMasterDetail = true;       // check if device with larger screen

        fragmentManager = getSupportFragmentManager();

        if (showMasterDetail){
            listFragment = new ListFragment();
            listFragment.setUserItemClickListener(this);
            fragmentManager.beginTransaction().add(R.id.list_frag_layout,listFragment).commit();
            detailsFragment = new DetailsFragment();
            detailsFragment.setUser(new User());
            fragmentManager.beginTransaction().add(R.id.details_frag_layout,detailsFragment).commit();
        }else{
            // show list fragment
            listFragment = new ListFragment();
            listFragment.setUserItemClickListener(this);
            fragmentManager.beginTransaction().add(R.id.main_fragment_container,listFragment).commit();
        }
        Log.d(TAG,"MainActivity onCreate()");
    }

    @Override
    public void onUserItemClick(User user) {
        detailsFragment = new DetailsFragment();
        detailsFragment.setUser(user);
        if (showMasterDetail){
            fragmentManager.beginTransaction().replace(R.id.details_frag_layout,detailsFragment).commit();
        }else{
            showListFragment = true;
            fragmentManager.beginTransaction().replace(R.id.main_fragment_container,detailsFragment).commit();
        }
    }

    @Override
    public void onBackPressed() {
        // if 'back' was clicked in details fragment show list fragment
        if (showListFragment) {
            fragmentManager.beginTransaction().replace(R.id.main_fragment_container,listFragment).commit();
            showListFragment = false;
        }else {
            super.onBackPressed();
        }
    }
}
