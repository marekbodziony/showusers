package marekbodziony.showusers;

import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

/**
 * Welcome screen
 */

public class WelcomeActivity extends AppCompatActivity {

    private ConstraintLayout welcomeScreenLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_welcome);

        // click anywhere to start
        welcomeScreenLayout = (ConstraintLayout) findViewById(R.id.welcome_layout);
        welcomeScreenLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent().setClass(getApplicationContext(),MainActivity.class));
            }
        });
    }
}
