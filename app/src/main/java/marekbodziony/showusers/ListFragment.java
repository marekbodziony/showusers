package marekbodziony.showusers;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import marekbodziony.showusers.model.SimpleUser;
import marekbodziony.showusers.model.User;
import marekbodziony.showusers.utils.NetworkUtils;
import marekbodziony.showusers.utils.UsersListAdapter;
import static marekbodziony.showusers.utils.Lib.TAG;
import static marekbodziony.showusers.utils.UsersListAdapter.UserClickListener;

public class ListFragment extends Fragment implements UsersListAdapter.UserClickListener{

    private ProgressBar progressBar;
    private TextView errorTextView;

    private UsersListAdapter adapter;
    private RecyclerView recyclerView;

    private List<SimpleUser> simpleUsersList;
    private List<SimpleUser> detailedUsersList;

    private static UserClickListener clickListener;

    // default constructor
    public ListFragment(){
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_list,container,false);

        progressBar = (ProgressBar) view.findViewById(R.id.fragment_list_progressBar);
        errorTextView = (TextView) view.findViewById(R.id.fragment_error_textView);

        simpleUsersList = new ArrayList<>();
        detailedUsersList = new ArrayList<>();

        adapter = new UsersListAdapter(getContext(), simpleUsersList, detailedUsersList,this);
        recyclerView = (RecyclerView) view.findViewById(R.id.fragment_list_recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);

        getUsersFromHttpInAsyncTaskAndDisplayOnList();

        return view;
    }

    // set click listener
    public void setUserItemClickListener(UserClickListener clickListener){
        this.clickListener = clickListener;
    }
    // get detailed users list
    public List<SimpleUser> getDetailedUsersList(){
        return detailedUsersList;
    }

    @Override
    public void onUserItemClick(User user) {
        clickListener.onUserItemClick(user);
    }

    // start AsyncTask, get users list from http and display it on a list
    private void getUsersFromHttpInAsyncTaskAndDisplayOnList(){
        new GetDataFromUrlAsyncTask().execute();
    }

    // AsyncTask used to get data from http response in background thread
    public class GetDataFromUrlAsyncTask extends AsyncTask<Void, Void, List<SimpleUser>[]> {

        @Override
        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
//            Log.d(TAG,"onPreExecute() \t| show progress bar");
        }

        @Override
        protected List<SimpleUser>[] doInBackground(Void... params) {
            List<SimpleUser> simple;
            List<SimpleUser> detail;
            List<SimpleUser>[] users = new ArrayList[2];
//            Log.d(TAG,"doInBackground()\t| start new thread, get data from http");
            simple = NetworkUtils.getSimpleUsersList();      // get list of simple users
            detail = NetworkUtils.getDetailedUsersList();    // get list of detailed users
            users[0] = simple;
            users[1] = detail;
            return users;
        }

        @Override
        protected void onPostExecute(List<SimpleUser>[] users) {
            progressBar.setVisibility(View.GONE);
            simpleUsersList.clear();
            simpleUsersList.addAll(users[0]);
            detailedUsersList.clear();
            detailedUsersList.addAll(users[1]);
            if (simpleUsersList == null || simpleUsersList.size() == 0 || detailedUsersList == null || detailedUsersList.size() ==0)
                errorTextView.setVisibility(View.VISIBLE);
//            Log.d(TAG,"onPostExecute() \t| hide progress bar, display list or error message");
        }
    }
}
