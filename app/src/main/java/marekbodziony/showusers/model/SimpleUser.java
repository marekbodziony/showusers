package marekbodziony.showusers.model;

/**
 * SimpleUser holds data from 'users.json'
 */

public class SimpleUser {

    private int id;
    private String name;

    //default constructor
    public SimpleUser(){}

    // constructor
    public SimpleUser(int id, String name) {
        this.id = id;
        this.name = name;
    }

    // getters and setters
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
}
