package marekbodziony.showusers.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 *  User holds data from 'details-users.json'
 */

public class User extends SimpleUser implements Parcelable{

    private String email;
    private int age;
    private String gender;
    private String[] hobbies;
    private String imageURL;
    private String backURL;

    //default constructor
    public User(){}

    // constructor
    public User(int id, String name, String email, int age, String gender, String[] hobbies, String imageURL, String backURL) {
        super(id, name);
        this.email = email;
        this.age = age;
        this.gender = gender;
        this.hobbies = hobbies;
        this.imageURL = imageURL;
        this.backURL = backURL;
    }

    // getters and setters
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }
    public String getGender() {
        return gender;
    }
    public void setGender(String gender) {
        this.gender = gender;
    }
    public String[] getHobbies() {
        return hobbies;
    }
    public void setHobbies(String[] hobbies) {
        this.hobbies = hobbies;
    }
    public String getImageURL() {
        return imageURL;
    }
    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }
    public String getBackURL() {
        return backURL;
    }
    public void setBackURL(String backURL) {
        this.backURL = backURL;
    }


    // ------------ for Parcelable implementation -----------------//
    // constructor
    public User (Parcel in){
        super(in.readInt(),in.readString());
        email = in.readString();
        age = in.readInt();
        gender = in.readString();
        in.readStringArray(hobbies);
        imageURL = in.readString();
        backURL = in.readString();
    }
    @Override
    public int describeContents() {return 0;}
    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(getId());
        out.writeString(getName());
        out.writeString(email);
        out.writeInt(age);
        out.writeString(gender);
        out.writeStringArray(hobbies);
        out.writeString(imageURL);
        out.writeString(backURL);
    }
    // Parcelable creator
    public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>(){

        @Override
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
}
