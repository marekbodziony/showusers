package marekbodziony.showusers;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import marekbodziony.showusers.model.User;
import static marekbodziony.showusers.utils.Lib.TAG;


public class DetailsFragment extends Fragment {

    private User user;

    private ImageView userImg, userBackgroundImg;
    private TextView name, email, age, gender, hobbies;
    private FloatingActionButton emailFab;

    // default constructor
    public DetailsFragment(){
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        Log.d(TAG,"DetailsFragment onCreate()");
        if (savedInstanceState != null) {
            user = savedInstanceState.getParcelable("user");
        }

        View view = inflater.inflate(R.layout.fragment_details,container,false);
        userImg = (ImageView) view.findViewById(R.id.details_image);
        userBackgroundImg = (ImageView) view.findViewById(R.id.details_background);
        name = (TextView) view.findViewById(R.id.details_name_val);
        email = (TextView) view.findViewById(R.id.details_email_val);
        age = (TextView) view.findViewById(R.id.details_age_val);
        gender = (TextView) view.findViewById(R.id.details_gender_val);
        hobbies = (TextView) view.findViewById(R.id.details_hobbies_val);
        emailFab = (FloatingActionButton) view.findViewById(R.id.details_email_fab);
        if (user.getEmail() != null) {
            emailFab.setVisibility(View.VISIBLE);
            emailFab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent sendEmail = new Intent(Intent.ACTION_SENDTO);
                    sendEmail.setData(Uri.parse("mailto:"));
                    sendEmail.putExtra(Intent.EXTRA_EMAIL, new String[]{email.getText().toString()});
                    startActivity(sendEmail);
                }
            });
        }
        displayUserDetails();
        return view;
    }

    // displays user details
    private void displayUserDetails(){
        if (user.getImageURL() != null){
            Glide.with(getContext()).load(user.getImageURL()).apply(RequestOptions.circleCropTransform()).into(userImg);
        }
        if(user.getBackURL() != null){
            Glide.with(getContext()).load(user.getBackURL()).into(userBackgroundImg);
            name.setTextColor(Color.WHITE);
            email.setTextColor(Color.WHITE);
        }
        name.setText(user.getName());
        email.setText(user.getEmail());
        if (user.getAge() > 0) age.setText(user.getAge()+"");
        if (user.getGender() != null) {
            gender.setText(user.getGender());
        }
        if (user.getHobbies() != null){
            hobbies.setText("");
            String[] hobby = user.getHobbies();
            for (int h = 0; h < hobby.length; h++){
                hobbies.append(hobby[h].toLowerCase() + ", ");
            }
        }
    }

    // set user
    public void setUser(User user){
        this.user = user;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelable("user",user);
    }
}
