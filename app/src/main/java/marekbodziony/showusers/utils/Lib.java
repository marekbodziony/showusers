package marekbodziony.showusers.utils;

/**
 * This class holds application static data.
 */

public class Lib {

    public static final String TAG = "UsersLog";
    public static final String DETAILED_USERS_URL = "https://raw.githubusercontent.com/whichapp/android-developer-test/master/user-details.json";
    public static final String SIMPLE_USERS_URL = "https://raw.githubusercontent.com/whichapp/android-developer-test/master/users.json";
}
