package marekbodziony.showusers.utils;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import marekbodziony.showusers.model.SimpleUser;
import marekbodziony.showusers.model.User;
import static marekbodziony.showusers.utils.Lib.*;

/**
 * NetworkUtils is responsible for getting JSON data from http and parsing to SimpleUser and User lists
 */

public class NetworkUtils{

    // public method for fetching SimpleUsers list form url (users.json)
    public static List<SimpleUser> getSimpleUsersList(){
        return geUserDataFromURL(Lib.SIMPLE_USERS_URL);
    }

    // public method for fetching Users list form url (users-details.json)
    public static List<SimpleUser> getDetailedUsersList(){
        return geUserDataFromURL(Lib.DETAILED_USERS_URL);
    }

    // gets data as JSON form URL
    private static List<SimpleUser> geUserDataFromURL(String usersUrl){

        List<SimpleUser> usersList = new ArrayList<>();
        try {
            URL url = new URL(usersUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            String json;

            try {
                InputStream inputStream = connection.getInputStream();

                Scanner scanner = new Scanner(inputStream);
                scanner.useDelimiter("\\A");

                if (scanner.hasNext()) {
                    json = scanner.next();
                    if (usersUrl.equals(Lib.SIMPLE_USERS_URL)){
                        usersList = parseJSONToSimpleUsersList(json);
                    }
                    else if (usersUrl.equals(Lib.DETAILED_USERS_URL)){
                        usersList = parseJSONToUsersList(json);
                    }
                }
                inputStream.close();
            } finally {
                connection.disconnect();
            }
        }catch (IOException e){
            Log.d(TAG,e.getMessage());
        }
        return usersList;
    }

    // parse JSON to simple users list
    private static List<SimpleUser> parseJSONToSimpleUsersList (String json){

        List<SimpleUser> users = new ArrayList<>();
        String name;
        int id;

        try {
            JSONArray usersJSON = new JSONObject(json).getJSONArray("users");
            for (int i = 0; i < usersJSON.length(); i ++){
                JSONObject j = (JSONObject)usersJSON.get(i);
                id = Integer.parseInt(j.keys().next());
                name = j.optString(""+id);
                SimpleUser user = new SimpleUser(id,name);
                users.add(user);
            }
        }
        catch (JSONException e) {
            Log.d(TAG,"Error! Can't parse JSON to users list");
            e.printStackTrace();
        }
        return users;
    }
    // parse JSON to users list
    private static List<SimpleUser> parseJSONToUsersList (String json){

        List<SimpleUser> users = new ArrayList<>();
        try {
            JSONArray usersJSON = new JSONObject(json).getJSONArray("users");
            for (int i = 0; i < usersJSON.length(); i ++){
                JSONObject j = (JSONObject)usersJSON.get(i);
                String name, email=null, gender=null, imageURL=null, backURL=null;
                int id = 0, age = 0;
                String[] hobbies = null;
                boolean hasGender = false;

                id = j.optInt("id");
                name = j.optString("name");
                if (!j.optString("email").equals(""))email = j.optString("email");
                age = j.optInt("age");
                if (j.keys().hasNext()){
                    Iterator<String> keys = j.keys();
                    for (int k = 0; k < j.length(); k++){
                        String key = keys.next();
                        if (key.equals("isFemale")) {
                            hasGender = true;
                            break;
                        }
                    }
                }
                if (hasGender){
                    if (j.optBoolean("isFemale")) gender = "female";
                    else gender = "male";
                }
                JSONArray hobbiesJSON = j.optJSONArray("hobbies");
                if (hobbiesJSON != null){
                    hobbies = new String[hobbiesJSON.length()];
                    for (int h = 0; h < hobbiesJSON.length(); h++){
                        hobbies[h] = (String)hobbiesJSON.get(h);
                    }
                }
                if (!j.optString("image").equals(""))imageURL = j.optString("image");
                if (!j.optString("back").equals(""))backURL = j.optString("back");
                User user = new User(id,name,email,age,gender,hobbies,imageURL,backURL);
                users.add(user);
            }
        }
        catch (JSONException e) {
            Log.d(TAG,"Error! Can't parse JSON to users list");
            e.printStackTrace();
        }
        return users;
    }
}
