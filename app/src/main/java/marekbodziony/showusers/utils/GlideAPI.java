package marekbodziony.showusers.utils;


import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;

/**
 * Ensures that Glide's generated API is created.
 * Needed for uploading images to ImageViews.
 */

@GlideModule
public final class GlideAPI extends AppGlideModule {}
