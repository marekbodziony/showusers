package marekbodziony.showusers.utils;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import marekbodziony.showusers.R;
import marekbodziony.showusers.model.SimpleUser;
import marekbodziony.showusers.model.User;

import static marekbodziony.showusers.utils.Lib.TAG;

public class UsersListAdapter extends RecyclerView.Adapter<UsersListAdapter.UserViewHolder>{

    private Context context;
    private List<SimpleUser> simpleUsersList;
    private List<SimpleUser> detailedUsersList;

    private static UserClickListener clickListener;

    // constructor
    public UsersListAdapter(Context context, List<SimpleUser> simpleUsersList, List<SimpleUser> detailedUsersList, UserClickListener clickListener) {
        this.context = context;
        this.simpleUsersList = simpleUsersList;
        this.detailedUsersList = detailedUsersList;
        this.clickListener = clickListener;
    }

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_single_item,parent,false);
        UserViewHolder holder = new UserViewHolder(view);
        return  holder;
    }

    @Override
    public void onBindViewHolder(UserViewHolder holder, int position) {
        SimpleUser simpleUser = simpleUsersList.get(position);
        //Log.d(TAG,"SimpleUser | id=" + simpleUser.getId() + ", name=" + simpleUser.getName());
        holder.id.setText(""+simpleUser.getId());
        holder.name.setText(simpleUser.getName());
    }

    @Override
    public int getItemCount() {
        return simpleUsersList.size();
    }

    // provide a reference to the views for each data item
    public class UserViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView id, name;

        public UserViewHolder(View itemView) {
            super(itemView);
            id = (TextView) itemView.findViewById(R.id.single_list_id);
            name = (TextView)  itemView.findViewById(R.id.single_list_name);
            itemView.setOnClickListener(this);
        }
        @Override
        public void onClick(View v) {
            int pos = getAdapterPosition();
            SimpleUser simple = simpleUsersList.get(pos);
            User user = null;
            for (SimpleUser u : detailedUsersList){
                if (u.getId() == simple.getId() && u.getName().equals(simple.getName())) {
                    user = (User)u;
//                    Log.d(TAG,"User       | id=" + user.getId() + ", name=" + user.getName() + ", email=" + user.getEmail() + ", age=" + user.getAge()
//                            + ", gender=" + user.getGender() + ", img=" + user.getImageURL() + ", back=" + user.getBackURL());
                }
            }
            if (user != null) clickListener.onUserItemClick(user);
            else Toast.makeText(context, "No such user", Toast.LENGTH_SHORT).show();
        }
    }

    // interface needed to communicate between list fragment and detail fragments
    public interface UserClickListener {
        void onUserItemClick(User user);
    }
}
