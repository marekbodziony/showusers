# ShowUsers #

"ShowUsers" is an Android app that displays list of users from API.


### Main features: ###

* present users on a list ([users.json](https://raw.githubusercontent.com/whichapp/android-developer-test/master/users.json))
* show user details when clicked on the list ([user-details.json](https://raw.githubusercontent.com/whichapp/android-developer-test/master/user-details.json))
* send email to the user (using default email application)